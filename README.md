# README #

The application consists of a single view controller displaying the exchange rate of Bitcoin over the last 28 days. The graph is implemented using a third-party library which also displays the current live Bitcoin value as a line over the graph. User can select and move along the values of the graph for more information. The app also caches the last values on file.

### Workspace ###

The workspace considers of the main project running the application and displaying the graph and a subproject BitcoinKit, which is the framework that handles all bitcoin related features, mainly fetching the historical data as well as the current exchange rate.

### Running ###

* Run the app with CMD+R
* Run the tests: CMD+U