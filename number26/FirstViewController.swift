//
//  FirstViewController.swift
//  number26
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import UIKit
import BitcoinKit
import Charts

class FirstViewController: UIViewController {

    @IBOutlet weak var chartView: BarChartView!
    
    private let updater = CurrencyUpdater(interval: 10, currency: .EUR)
    private var historicalData: HistoricalData!
    private let persistence = FilePersistence()
    
    private var currentValueLine: ChartLimitLine? = nil
    private var selectedRateLine: ChartLimitLine? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Bitoin Price Index"
        chartView.noDataText = "Loading data ..."
        chartView.delegate = self
        chartView.descriptionText = "Cached values"
        historicalData = persistence.readFromLog()
        showChart()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        loadHistory()
    }
    
    private func loadHistory() {
        HistoricalDataUpdater(currency: .EUR).fetchHistory { [weak self] (data, error) in
            guard let `self` = self, data = data else {
                print(error)
                return
            }
            self.chartView.descriptionText = "Up to date values"
            self.historicalData = data
            self.showChart()
            
            self.updateCurrentRate()
        }
    }
    
    private func updateCurrentRate() {
        self.updater.start { [weak self] exchangeRate in
            guard let `self` = self else { return }
            if let latestRate = self.updater.latestRate {
                if self.currentValueLine != nil {
                    self.chartView.rightAxis.removeLimitLine(self.currentValueLine!)
                }
                self.currentValueLine = ChartLimitLine(limit: Double(latestRate.rate), label: "Current \(latestRate.rate)")
                self.chartView.rightAxis.addLimitLine(self.currentValueLine!)
            }
            self.historicalData.updateLatestRate(exchangeRate)
            self.persistence.writeToLog(self.historicalData)
            self.showChart()
        }
    }

}

extension FirstViewController: ChartViewDelegate {
    
    // MARK: - Chart specifics
    
    private func showChart() {
        var dateLabels = [String]()
        var rateValues = [Double]()
        
        for exchangeRate in historicalData.dailyExchangeRates {
            dateLabels.append(exchangeRate.date.toString())
            rateValues.append(exchangeRate.rate)
        }
        
        var dataEntries: [BarChartDataEntry] = []
        for i in 0..<dateLabels.count {
            let dataEntry = BarChartDataEntry(value: rateValues[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: "Bitcoin Rate")
        let chartData = BarChartData(xVals: dateLabels, dataSet: chartDataSet)
        chartView.data = chartData
        chartView.xAxis.labelPosition = .Bottom
        chartView.animate(xAxisDuration: 1, yAxisDuration: 1)
    }
    
    private func removeLine(chartLine: ChartLimitLine?) {
        if chartLine != nil {
            self.chartView.rightAxis.removeLimitLine(chartLine!)
        }
    }
    
    private func addLineForRate(AtIndex index: Int) {
        let selectedRate = historicalData.dailyExchangeRates[index]
        selectedRateLine = ChartLimitLine(limit: Double(selectedRate.rate), label: "Day: \(selectedRate.date.toString()) - Rate \(selectedRate.rate)")
        selectedRateLine!.lineColor = UIColor.orangeColor()
        self.chartView.rightAxis.addLimitLine(selectedRateLine!)
    }
    
    // MARK: - ChartViewDelegate
    
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
        removeLine(selectedRateLine)
        addLineForRate(AtIndex: entry.xIndex)
    }
    
    func chartValueNothingSelected(chartView: ChartViewBase) {
        removeLine(selectedRateLine)
    }
    
}

