//
//  Networking.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

class Networking {
    
    static func request(withEndpoint endpoint: Endpoint, params: [String: String]? = nil, completion: (AnyObject?, NSError?) -> Void) {
        if let request = RequestBuilder.build(endpoint, params: params) {
            URLSession().httpGet(request, callback: { (data, error) -> Void in
                if let error = error {
                    print("error: \(error.localizedDescription): \(error.userInfo)")
                }
                else if let data = data {
                    do {
                        let json: AnyObject = try NSJSONSerialization.JSONObjectWithData(data, options: [])
                        completion(json, nil)
                    } catch let aError as NSError {
                        completion(nil, aError)
                    }
                }
            })
        }
        else {
            print("Unable to create NSURL")
        }
    }
}

enum Endpoint {
    case HistoricalData
    case CurrentPrice(currency: Currency)
    
    var path : String {
        switch self {
        case .HistoricalData: return "/historical/close.json"
        case let .CurrentPrice(currency): return "/currentprice/\(currency).json"
        }
    }
}



private class RequestBuilder {
    
    struct Constants {
        private static let scheme = "https"
        private static let host = "api.coindesk.com"
        private static let apiPath = "/v1/bpi"
    }
    
    static func build(endpoint: Endpoint, params: [String: String]? = nil) -> NSURLRequest? {
        let components = NSURLComponents()
        components.scheme = Constants.scheme
        components.host = Constants.host
        components.path = "\(Constants.apiPath)\(endpoint.path)"
        var queryItems = [NSURLQueryItem]()
        params?.forEach { param in
            queryItems.append(NSURLQueryItem(name: param.0, value: param.1))
        }
        components.queryItems = queryItems
        guard let url = components.URL else { return nil }
        return NSMutableURLRequest(URL: url)
    }
}


private class URLSession: NSObject, NSURLSessionDelegate, NSURLSessionTaskDelegate {
    typealias CallbackBlock = (data: NSData?, error: NSError?) -> Void
    
    func httpGet(request: NSURLRequest!, callback: CallbackBlock) {
        let configuration =
            NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: configuration,
                                   delegate: self,
                                   delegateQueue:NSOperationQueue.mainQueue())
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            callback(data: data, error: error)
        }
        task.resume()
    }
    
    @objc func URLSession(session: NSURLSession, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        completionHandler(
            NSURLSessionAuthChallengeDisposition.UseCredential,
            NSURLCredential(forTrust:
                challenge.protectionSpace.serverTrust!))
    }
    
    @objc func URLSession(session: NSURLSession, task: NSURLSessionTask, willPerformHTTPRedirection response: NSHTTPURLResponse, newRequest request: NSURLRequest, completionHandler: (NSURLRequest?) -> Void) {
        completionHandler(request)
    }
}