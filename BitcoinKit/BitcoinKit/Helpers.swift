//
//  Helpers.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

public extension NSDate {
    
    func days(fromDate date: NSDate = NSDate()) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: self, toDate: date, options: []).day
    }
    
    static var outputFormat: String { return "MM/d" }
    static var inputFormat: String { return "yyyy-MM-dd" }
    
    static func dateFormatter(format: String) -> NSDateFormatter {
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        return formatter
    }
    
    public func toString() -> String {
        return NSDate.dateFormatter(NSDate.outputFormat).stringFromDate(self)
    }
    
    static func fromString(string: String) -> NSDate? {
        return dateFormatter(inputFormat).dateFromString(string)
    }
    
}