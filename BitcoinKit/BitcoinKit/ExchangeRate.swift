//
//  Exchange Rate.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

public final class ExchangeRate: NSObject {
    public private(set) var date: NSDate
    public private(set) var rate: Double
    
    public init(date: NSDate, rate: Double) {
        self.date = date
        self.rate = rate
    }
}

extension ExchangeRate: NSCoding {
    
    @objc convenience public init?(coder aDecoder: NSCoder) {
        guard let date = aDecoder.decodeObjectForKey("date") as? NSDate else {
            return nil
        }
        self.init(date: date, rate: aDecoder.decodeDoubleForKey("rate"))
    }
    
    @objc public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.date, forKey: "date")
        aCoder.encodeDouble(self.rate, forKey: "rate")
    }
}


public func ==(lhs: ExchangeRate, rhs: ExchangeRate) -> Bool {
    guard lhs.date.toString() == rhs.date.toString() else { return false }
    guard lhs.rate == rhs.rate else { return false }
    return true
}