//
//  DataIOStream.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

public class FilePersistence {
    
    public init() {}
    
    private var logPath: NSURL {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory: String = paths[0]
        let directoryURL = NSURL(string: documentsDirectory)!.URLByAppendingPathComponent("HistoricalData")
        let logPathURL = directoryURL.URLByAppendingPathComponent("Logs")
        if !NSFileManager.defaultManager().fileExistsAtPath(logPathURL.absoluteString) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(logPathURL.absoluteString, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return logPathURL.URLByAppendingPathComponent("history.log")
    }
    
    public func writeToLog(historicalData: HistoricalData) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { [weak self] in
            guard let `self` = self else { return }
            let path = self.logPath.absoluteString
            NSKeyedArchiver.archiveRootObject(historicalData, toFile: path)
        }
    }
    
    public func readFromLog() -> HistoricalData {
        let path = logPath.absoluteString
        guard let historicalData = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? HistoricalData else {
            return HistoricalData()
        }
        return historicalData
    }
    
}