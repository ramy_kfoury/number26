//
//  CurrencyParser.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

public enum Currency: String {
    case EUR
}

struct CurrentPriceParser {
    
    let json: [String: AnyObject]
    
    func parse(currency: Currency) -> ExchangeRate? {
        guard let bpi = json["bpi"] as? [String: AnyObject] else { return nil }
        guard let exchangeRate = bpi[currency.rawValue] as? [String: AnyObject] else { return nil }
        guard let rate = exchangeRate["rate_float"] as? Double else { return nil }
        return ExchangeRate(date: NSDate(), rate: rate)
    }
    
}