//
//  Updater.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

public struct HistoricalDataUpdater {
    
    public let currency: Currency
    
    public init(currency: Currency) {
        self.currency = currency
    }
    
    public func fetchHistory(completion: ((HistoricalData?, NSError?) -> Void)? = nil) {
        Networking.request(withEndpoint: Endpoint.HistoricalData, params: ["currency": Currency.EUR.rawValue]) { (json, error) in
            if let error = error {
                completion?(nil, error) // TODO: handle error appropriately
                return
            }
            guard let json = json as? [String: AnyObject] else {
                // TODO: handle json error appropriately
                return
            }
            completion?(HistoricalData(json: json), nil)
        }
    }
}

public class CurrencyUpdater {
    
    public let interval: NSTimeInterval
    public let currency: Currency
    
    public var latestRate: ExchangeRate? {
        didSet {
            if let latestRate = latestRate {
                self.onUpdate?(latestRate)
            }
        }
    }
    var timer: NSTimer!
    var onUpdate: (ExchangeRate -> Void)?
    
    required public init(interval: NSTimeInterval, currency: Currency, latestRate: ExchangeRate? = nil) {
        self.interval = interval
        self.currency = currency
        self.latestRate = latestRate
    }
    
    @objc func update() {
        Networking.request(withEndpoint: Endpoint.CurrentPrice(currency: currency)) { [weak self] (json, error) in
            if let error = error {
                print(error) // TODO: handle error appropriately
                return
            }
            guard let `self` = self, json = json as? [String: AnyObject] else {
                // TODO: handle json error appropriately
                return
            }
            self.latestRate = CurrentPriceParser(json: json).parse(self.currency)
        }
        
        
    }
    
    public func start(onUpdate: (ExchangeRate -> Void)? = nil) {
        self.onUpdate = onUpdate
        self.timer = NSTimer.scheduledTimerWithTimeInterval(interval, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        self.timer.fire()
    }
    
    public func stop() {
        self.timer?.invalidate()
        self.timer = nil
    }
}
