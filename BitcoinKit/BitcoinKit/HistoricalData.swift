//
//  Models.swift
//  BitcoinKit
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import Foundation

public final class HistoricalData: NSObject {
    
    public private(set) var dailyExchangeRates: [ExchangeRate]
    
    public required init(dailyExchangeRates: [ExchangeRate]) {
        self.dailyExchangeRates = dailyExchangeRates
    }
    
    public init(json: [String: AnyObject]? = nil) {
        guard let bpi = json?["bpi"] as? [String: Double] else { self.dailyExchangeRates = []; return }
        
        let today = NSDate()
        var dailyExchangeRates = [ExchangeRate]()
        bpi.forEach { (shortDate, rate) in
            if let date = NSDate.fromString(shortDate) where date.days(fromDate: today) <= 28  {
                dailyExchangeRates.append(ExchangeRate(date: date, rate: rate))
            }
        }
        self.dailyExchangeRates = dailyExchangeRates.sort({ (lhs, rhs) -> Bool in
            return lhs.date.compare(rhs.date) == NSComparisonResult.OrderedAscending
        })
    }
    
    public func updateLatestRate(latestRate: ExchangeRate) {
        guard !dailyExchangeRates.isEmpty else { return }
        if let matchingDayIndex = dailyExchangeRates.indexOf({ rate in
            rate.date.toString() == latestRate.date.toString()
        }) {
            dailyExchangeRates[matchingDayIndex] = latestRate
        } else {
            dailyExchangeRates.append(latestRate)
        }
        
    }
}

extension HistoricalData: NSCoding {
    
    @objc convenience public init?(coder aDecoder: NSCoder) {
        guard let dailyExchangeRates = aDecoder.decodeObjectForKey("rates") as? [ExchangeRate] else {
            return nil
        }
        self.init(dailyExchangeRates: dailyExchangeRates)
    }
    
    @objc public func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.dailyExchangeRates, forKey: "rates")
    }
}



public func ==(lhs: HistoricalData, rhs: HistoricalData) -> Bool {
    return lhs.dailyExchangeRates == rhs.dailyExchangeRates
}