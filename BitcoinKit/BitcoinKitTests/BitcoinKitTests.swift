//
//  BitcoinKitTests.swift
//  BitcoinKitTests
//
//  Created by Ramy Kfoury on 04/06/16.
//  Copyright © 2016 Ramy Kfoury. All rights reserved.
//

import XCTest
@testable import BitcoinKit

class BitcoinKitTests: XCTestCase {
    
    func testWhenCreatingHistoricalDataWithNoJson_thenBitCoinPriceIndexIsEmpty() {
        let historicalData = HistoricalData()
        
        XCTAssertNotNil(historicalData.dailyExchangeRates)
        XCTAssertEqual(historicalData.dailyExchangeRates.count, 0)
    }
    
    func testCanParseHistoricalResponse() {
        let historicalData = parseHistoricalData()
        
        XCTAssertNotNil(historicalData.dailyExchangeRates)
        XCTAssertEqual(historicalData.dailyExchangeRates.count, 28)
    }
    
    func testCanPersistHistoricalResponse() {
        let historicalData = parseHistoricalData()
        
        FilePersistence().writeToLog(historicalData)
        
        let persistedHistoricalData = FilePersistence().readFromLog()
        
        XCTAssertNotNil(persistedHistoricalData.dailyExchangeRates)
        XCTAssertEqual(persistedHistoricalData.dailyExchangeRates.count, 28)
    }
    
    func testCanParseOneExchangeRate() {
        let jsonResult = FileReader.json(fromFile: "CurrentJsonMock0", fromBundle: NSBundle(forClass: self.dynamicType))
        let parser = CurrentPriceParser(json: jsonResult)
        let exchangeRate = parser.parse(.EUR)
        
        XCTAssertEqual(exchangeRate?.date.toString(), NSDate().toString())
        XCTAssertEqual(exchangeRate?.rate, 514.7745)
    }
    
    func testCurrencyUpdaterCanPollExactAmountOfRatesInSpecifiedTime() {
        let currencyUpdaterExpectation = expectationWithDescription("updating currency rate expectation")
        
        let updater = MockCurrencyUpdater(interval: 1, currency: .EUR)
        var exchangeRates = [ExchangeRate]()
        updater.start { (exchangeRate) in
            exchangeRates.append(exchangeRate)
            if exchangeRates.count == 5 {
                updater.stop()
                currencyUpdaterExpectation.fulfill()
            }
        }
        
        waitForExpectationsWithTimeout(6) { (error) in
            XCTAssertNil(error)
            
            XCTAssertEqual(exchangeRates[0].rate, 514.7745)
            XCTAssertEqual(exchangeRates[1].rate, 515.7745)
            XCTAssertEqual(exchangeRates[2].rate, 516.7745)
            XCTAssertEqual(exchangeRates[3].rate, 517.7745)
            XCTAssertEqual(exchangeRates[4].rate, 518.7745)
        }
    }
    
    func testWhenCurrencyUpdaterFetchesLastValue_thenHistoricalDataIsUpdatedWithLastValue() {
        let historicalData = parseHistoricalData()
        let currencyUpdaterExpectation = expectationWithDescription("updating currency rate expectation")
        
        let updater = MockCurrencyUpdater(interval: 1, currency: .EUR)
        var latestRate: ExchangeRate!
        updater.start { (exchangeRate) in
            updater.stop()
            latestRate = exchangeRate
            historicalData.updateLatestRate(exchangeRate)
            currencyUpdaterExpectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(2) { (error) in
            XCTAssertNil(error)
            XCTAssertEqual(latestRate, historicalData.dailyExchangeRates.last!)
        }
    }
    
    private func parseHistoricalData() -> HistoricalData {
        let jsonResult = FileReader.json(fromFile: "HistoricalJsonMock", fromBundle: NSBundle(forClass: self.dynamicType))
        return HistoricalData(json: jsonResult)
    }
    
}

private class MockCurrencyUpdater: CurrencyUpdater {
    
    var currentIteration = 0
        
    override func update() {
        let jsonResult = FileReader.json(fromFile: "CurrentJsonMock\(currentIteration)", fromBundle: NSBundle(forClass: self.dynamicType))
        currentIteration += 1
        let parser = CurrentPriceParser(json: jsonResult)
        latestRate = parser.parse(.EUR)!
    }
}

private class FileReader {
    
    static func json(fromFile filename: String, fromBundle bundle: NSBundle? = nil) -> [String: AnyObject] {
        let bundle = bundle ??  NSBundle(forClass: self)
        let file = bundle.pathForResource(filename, ofType: "json")!
        let jsonData = try! NSData(contentsOfFile: file, options: .DataReadingMappedIfSafe)
        return try! NSJSONSerialization.JSONObjectWithData(jsonData, options: .MutableContainers) as! [String: AnyObject]
    }
}
